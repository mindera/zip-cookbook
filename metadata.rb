name             'zip'
maintainer       'Mindera'
maintainer_email 'miguel.fonseca@mindera.com'
license          'Apache 2.0'
description      'Generic provider to deploy zip files'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.0.1'

supports 'centos', '~> 6.0'
supports 'amazon'
