if defined?(ChefSpec)
  def install_zip_file(name)
    ChefSpec::Matchers::ResourceMatcher.new(:zip_file, :install, name)
  end

  def install_zip_file_from_snapshot(name)
    ChefSpec::Matchers::ResourceMatcher.new(:zip_file_from_snapshot, :install, name)
  end
end
